#include "FastLED.h"
#include "Arduino.h"
#define NUM_LEDS 49
#define PIN_LED1 D7
#define PIN_LED2 D6
#define PIN_SUB_LED3 D5
#define LIGHT_SENSOR_PIN D2
#define V110_SENSOR_PIN A0
#define PHOTOCELL_1 D1
#define PHOTOCELL_2 D4

CRGB leds1[NUM_LEDS];
CRGB leds2[NUM_LEDS];
CRGB leds3[NUM_LEDS];

String LOW_STATE = "0";
String MED_STATE = "1";
String HIGH_STATE = "2";

const int statesCases = 16;
const String stateArray[][3] = {
    {"0000", "0", "0"},
    {"0001", "0", "2"},
    {"0010", "0", "2"},
    {"0011", "0", "2"},
    {"0100", "0", "2"},
    {"0101", "2", "2"},
    {"0110", "1", "2"},
    {"0111", "2", "2"},
    {"1000", "0", "0"},
    {"1001", "1", "1"},
    {"1010", "0", "1"},
    {"1011", "1", "1"},
    {"1100", "0", "1"},
    {"1101", "2", "1"},
    {"1110", "1", "1"},
    {"1111", "2", "1"},

};

bool ledStatus = false;
bool zone1 = false;
bool zone2 = false;

float zone1onSince = 0; //ms
float zone2onSince = 0; //ms

float timerAutoOff = 120000; //ms

String topLightStatus = "";
String bottomLightStatus = "";

int topCount = 1;
int bottomCount = 1;


void setup()
{
    Serial.begin(115200);
    Serial.println("start");
    FastLED.addLeds<NEOPIXEL, PIN_LED1>(leds1, NUM_LEDS).setCorrection(TypicalSMD5050);
    FastLED.addLeds<NEOPIXEL, PIN_LED2>(leds2, NUM_LEDS).setCorrection(TypicalSMD5050);
    FastLED.addLeds<NEOPIXEL, PIN_SUB_LED3>(leds3, NUM_LEDS).setCorrection(TypicalSMD5050);

    pinMode(LED_BUILTIN, OUTPUT);
    pinMode(LIGHT_SENSOR_PIN, INPUT);
    pinMode(V110_SENSOR_PIN, INPUT);
    pinMode(PHOTOCELL_1, INPUT);
    pinMode(PHOTOCELL_2, INPUT);
    attachInterrupt(digitalPinToInterrupt(PHOTOCELL_1), photocell1Activated, RISING);
    attachInterrupt(digitalPinToInterrupt(PHOTOCELL_2), photocell2Activated, RISING);

    setBlack(leds1);
    setBlack(leds2);
    setBlack(leds3);

    for (size_t i = 0; i < 5; i++)
    {
        leds1[48] = CRGB::Black;
        delay(500);
        leds1[48] = CRGB::Gold;
        delay(500);
    }
    setBlack(leds1);
}

void loop()
{
    delay(10);
    int sunDown = digitalRead(LIGHT_SENSOR_PIN) == 0;
    int v110Raw = analogRead(V110_SENSOR_PIN);
    int v110Status = v110Raw < 800;
    int zone1 = zone1onSince + timerAutoOff > millis();
    int zone2 = zone2onSince + timerAutoOff > millis();
    String finalstate = String(sunDown, DEC) + String(v110Status, DEC) + String(zone1, DEC) + String(zone2, DEC);
    Serial.print("finalstate ");
    Serial.print(finalstate);

    String previousTopLightStatus = topLightStatus;
    String previousBottomLightStatus = bottomLightStatus;

    topLightStatus = "";
    bottomLightStatus = "";

    for (int i = 0; i < statesCases; i++)
    {
        String currentCase = stateArray[i][0];
        if (currentCase.equals(finalstate))
        {
            topLightStatus = stateArray[i][1];
            bottomLightStatus = stateArray[i][2];
            break;
        }
    }

    Serial.print(" - ");
    Serial.print(topLightStatus);
    Serial.print(" - ");
    Serial.print(bottomLightStatus);
    Serial.print(" - 110V : ");
    Serial.println(v110Raw);

    topCount++;
    if (!previousTopLightStatus.equals(topLightStatus))
    {
        topCount = 1;
    }

    int currentTopCount;
    if (topCount / 3 > NUM_LEDS)
    {
        currentTopCount = NUM_LEDS;
    }
    else
    {
        currentTopCount = topCount / 3;
    }

    Serial.print(" - ");
    Serial.println(currentTopCount);


    if (topLightStatus.equals(LOW_STATE))
    {
        turnToColor(20, CRGB::Black, leds1, currentTopCount);
        turnToColor(20, CRGB::Black, leds2, currentTopCount);
    }
    else if (topLightStatus.equals(MED_STATE))
    {
        turnToColor(20, CRGB::Gold, leds1, currentTopCount);
        turnToColor(20, CRGB::Gold, leds2, currentTopCount);
    }
    else if (topLightStatus.equals(HIGH_STATE))
    {
        turnToColor(20, CRGB(255, 255, 170), leds1, currentTopCount);
        turnToColor(20, CRGB(255, 255, 170), leds2, currentTopCount);
    }

    if (bottomLightStatus.equals(LOW_STATE))
    {
        turnToColor(10, CRGB::Black, leds3, NUM_LEDS);
    }
    else if (bottomLightStatus.equals(MED_STATE))
    {
        turnToColor(10, CRGB::Gold, leds3, NUM_LEDS);
    }
    else if (bottomLightStatus.equals(HIGH_STATE))
    {
        turnToColor(2, CRGB(10, 9, 0), leds3, NUM_LEDS);
    }
}

void photocell1Activated()
{
    zone1onSince = millis();
    Serial.print("photocell1Activated ");
}

void photocell2Activated()
{
    zone2onSince = millis();
    Serial.print("photocell2Activated ");
}

void turnToColor(double meteorTrailDecay, CRGB color, CRGB *leds, int maxValue)
{
    // Serial.print("maxValue ");
    // Serial.print(maxValue);

    for (int i = 0; i < min(NUM_LEDS, maxValue); i++)
    {
        if (random(10) > 8)
        {
            shadeToColor(i, meteorTrailDecay, color, leds);
        }
    }
    FastLED.show();
}

void fadeToBlack(int ledNo, byte fadeValue, CRGB *leds)
{
    leds[ledNo].fadeToBlackBy(fadeValue);
}

void shadeToColor(int ledNo, int fadeValue, CRGB targetColor, CRGB *leds)
{
    double currentRed = leds[ledNo].red;
    double currentGreen = leds[ledNo].green;
    double currentBlue = leds[ledNo].blue;

    double absRedDiff = abs(currentRed - targetColor.red);
    double absGreenDiff = abs(currentGreen - targetColor.green);
    double absBlueDiff = abs(currentBlue - targetColor.blue);

    double maxDiff = max(max(absRedDiff, absGreenDiff), absBlueDiff);

    double variation = 0;
    if (maxDiff != 0)
    {
        variation = fadeValue / maxDiff;
    }

    double signRed = sign(targetColor.red - currentRed);
    double signGreen = sign(targetColor.green - currentGreen);
    double signBlue = sign(targetColor.blue - currentBlue);

    double possibleRed = currentRed + variation * absRedDiff * signRed;
    double possibleGreen = currentGreen + variation * absGreenDiff * signGreen;
    double possibleBlue = currentBlue + variation * absBlueDiff * signBlue;

    double newRed = colorMaxSelection(targetColor.red, currentRed, possibleRed);
    double newGreen = colorMaxSelection(targetColor.green, currentGreen, possibleGreen);
    double newBlue = colorMaxSelection(targetColor.blue, currentBlue, possibleBlue);
    leds[ledNo].red = newRed;
    leds[ledNo].green = newGreen;
    leds[ledNo].blue = newBlue;

    // if (ledNo == 4)
    // {
    //     Serial.print(" - variation ");
    //     Serial.print(variation);
    //     Serial.print(" - maxDiff ");
    //     Serial.print(maxDiff);
    //     Serial.print(" - fadeValue ");
    //     Serial.println(fadeValue);

    //     Serial.print("targetColor.red ");
    //     Serial.print(targetColor.red);
    //     Serial.print(" - currentRed ");
    //     Serial.print(currentRed);
    //     Serial.print(" - signRed ");
    //     Serial.print(signRed);
    //     Serial.print(" - absRedDiff ");
    //     Serial.print(absRedDiff);
    //     Serial.print(" - possibleRed ");
    //     Serial.print(possibleRed);
    //     Serial.print(" - newRed ");
    //     Serial.print(newRed);
    //     Serial.print(" - leds[ledNo].red ");
    //     Serial.println(leds[ledNo].red);

    //     Serial.print("targetColor.green ");
    //     Serial.print(targetColor.green);
    //     Serial.print(" - currentGreen ");
    //     Serial.print(currentRed);
    //     Serial.print(" - signGreen ");
    //     Serial.print(signGreen);
    //     Serial.print(" - absGreenDiff ");
    //     Serial.print(absGreenDiff);
    //     Serial.print(" - possibleGreen ");
    //     Serial.print(possibleGreen);
    //     Serial.print(" - newGreen ");
    //     Serial.print(newGreen);
    //     Serial.print(" - leds[ledNo].red ");
    //     Serial.println(leds[ledNo].Green);

    //     Serial.print("targetColor.blue " );
    //     Serial.print(targetColor.blue);
    //     Serial.print(" - currentblue ");
    //     Serial.print(currentBlue);
    //     Serial.print(" - signBlue ");
    //     Serial.print(signBlue);
    //     Serial.print(" - absBlueDiff ");
    //     Serial.print(absBlueDiff);
    //     Serial.print(" - possibleBlue ");
    //     Serial.print(possibleBlue);
    //     Serial.print(" - newBlue ");
    //     Serial.print(newBlue);
    //     Serial.print(" - leds[ledNo].Blue ");
    //     Serial.println(leds[ledNo].blue);
    // }
}

double newColorIncrementation(double targetColor, double currentColor, double fadeValue)
{
    return currentColor + (max(targetColor, currentColor) * sign(targetColor - currentColor) * fadeValue);
}

double colorMaxSelection(double targetColor, double currentColor, double possibleNewColor)
{
    if (sign(targetColor - currentColor) < 0)
    {
        return max(possibleNewColor, targetColor);
    }
    else
    {
        return min(possibleNewColor, targetColor);
    }
}

double sign(double value)
{
    return (value > 0) - (value < 0);
}

void setPixel(int Pixel, CRGB color, CRGB *leds)
{
    leds[Pixel] = color;
}

void setBlack(CRGB *leds)
{
    for (int i = 0; i < NUM_LEDS; i++)
    {
        leds[i].r = 0;
        leds[i].g = 0;
        leds[i].b = 0;
    }
    FastLED.show();
}